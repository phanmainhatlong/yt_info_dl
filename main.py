import tkinter as tk
import math
import os
import subprocess
from pytube import YouTube
from tkinter import ttk
from threading import Thread
from pathlib import Path
from tkinter import messagebox as mb
from tkinter import filedialog as fd
from scripts.constants import *
from scripts.get_link import GET_LINK

# TODO: Complete the UI, from line 101


class GUI:
    def __init__(self, master, verified_url) -> None:
        self.root = master
        self.root.title("Youtube Downloader")
        self.root.geometry(
            f"{WIN_WIDTH}x{WIN_HEIGHT}+{int(self.root.winfo_screenwidth() / 2 - WIN_WIDTH / 2)}+{int(self.root.winfo_screenheight() / 2 - WIN_HEIGHT / 2)}")
        self.root.resizable(width=False, height=False)
        self.root.protocol("WM_DELETE_WINDOW",
                           self.handle_unexpected_close_event)

        self.yt_url = tk.StringVar(self.root, verified_url)
        if not self.yt_url.get():
            self.handle_close_event()
            return
        self.video_object = "placeholder"
        self.video_info = {
            "title": tk.StringVar(self.root, "Loading..."),
            "length_s": tk.StringVar(self.root, "00"),
            "length_m": tk.StringVar(self.root, "00:00"),
            "length_h": tk.StringVar(self.root, "00:00:00"),
            "views": tk.IntVar(self.root, 0),
            "author": tk.StringVar(self.root, "Loading..."),
            "description": tk.StringVar(self.root, "Loading...")
        }
        self.video_download_resolution = {}
        self.time_display = "seconds"
        self.unexpected_kill_flag = False

        Thread(target=self.init_data).start()
        self.init_widget()
        self.draw_widget()

    def init_data(self):
        self.video_object = YouTube(self.yt_url.get(),
                                    on_complete_callback=self.on_complete_dl,
                                    on_progress_callback=self.on_progress_dl)
        self.streams = self.video_object.streams

        for stream in self.streams:
            self.video_download_resolution[f"{stream.resolution}/{stream.mime_type}"] = stream.itag
        seconds = self.video_object.length
        if seconds > 60 and seconds < 3600:
            self.time_display = "minutes"
        elif seconds > 3600:
            self.time_display = "hours"
        else:
            self.time_display = "seconds"
        hours = math.floor(seconds / 3600)
        minutes = math.floor((seconds - (hours * 3600)) / 60)
        seconds = seconds - (hours * 3600) - (minutes * 60)
        self.video_info["title"].set(self.video_object.title)
        self.video_info["length_h"].set(
            f"{hours}:{minutes}:{seconds}")
        self.video_info["length_m"].set(
            f"{minutes}:{seconds}")
        self.video_info["length_s"].set(
            f"{seconds}")
        self.video_info["views"].set(self.video_object.views)
        self.video_info["author"].set(self.video_object.author)
        self.video_info["description"].set(self.video_object.description)

        self.config_widget()

    def init_widget(self):
        self.tab_master = ttk.Notebook(self.root)

        self.tab_info = ttk.Frame(self.tab_master, width=600, height=580)
        self.title_label = ttk.Entry(self.tab_info, textvariable=self.video_info['title'], font=(
            "times", 16), state=tk.DISABLED)
        self.length_label = ttk.Entry(self.tab_info, textvariable=self.video_info['length_h'] if self.time_display == "hours" else
                                      self.video_info['length_m'] if self.time_display == "minutes" else
                                      self.video_info['length_s'], font=("times", 16), state=tk.DISABLED)
        self.views_label = ttk.Entry(
            self.tab_info, text=self.video_info['views'], font=("times", 16), state=tk.DISABLED)
        self.author_label = ttk.Entry(
            self.tab_info, text=self.video_info['author'], font=("times", 16), state=tk.DISABLED)
        self.description_text = tk.Text(self.tab_info)
        self.title_xscrollbar = tk.Scrollbar(
            self.tab_info, orient=tk.HORIZONTAL, command=self.title_label.xview)
        self.description_yscrollbar = tk.Scrollbar(
            self.tab_info, command=self.description_text.yview)

        self.tab_download = ttk.Frame(self.tab_master, width=600, height=580)
        self.resolution_select = tk.Listbox(self.tab_download)
        self.dl_progress_bar = ttk.Progressbar(
            self.tab_download, orient=tk.HORIZONTAL, length=580, mode="determinate")

    def config_widget(self):
        self.tab_master.add(self.tab_info, text="Info")
        self.tab_master.add(self.tab_download, text="Download")
        self.description_text.insert(
            tk.END, self.video_info['description'].get())
        self.title_label.config(xscrollcommand=self.title_xscrollbar.set)
        self.description_text.config(
            yscrollcommand=self.description_yscrollbar.set)
        for _, e in enumerate(self.video_download_resolution):
            self.resolution_select.insert(tk.END, e)
        self.resolution_select.bind(
            "<Double-Button>", lambda x: Thread(target=self.download_video).start())

    def draw_widget(self):
        self.tab_master.place(x=0, y=0, width=WIN_WIDTH, height=WIN_HEIGHT-20)

        self.tab_info.pack(fill=tk.BOTH, expand=True)
        ttk.Label(self.tab_info, text="Title:", font=("times", 16)
                  ).place(x=10, y=10, width=50, height=50)
        ttk.Label(self.tab_info, text="Length:", font=(
            "times", 16)).place(x=10, y=70, width=65, height=50)
        ttk.Label(self.tab_info, text="Views:", font=(
            "times", 16)).place(x=10, y=130, width=65, height=50)
        ttk.Label(self.tab_info, text="Author:", font=(
            "times", 16)).place(x=10, y=190, width=70, height=50)
        ttk.Label(self.tab_info, text="Description:", font=(
            "times", 16)).place(x=10, y=250, width=110, height=50)
        self.title_label.place(x=120, y=10, width=460, height=50)
        self.title_xscrollbar.place(x=120, y=60, width=460, height=5)
        self.length_label.place(x=120, y=70, width=460, height=50)
        self.views_label.place(x=120, y=130, width=460, height=50)
        self.author_label.place(x=120, y=190, width=460, height=50)
        self.description_text.place(x=120, y=250, width=460, height=300)
        self.description_yscrollbar.place(
            relx=1, rely=1, relwidth=1, height=50)

        self.tab_download.pack(fill=tk.BOTH, expand=True)
        ttk.Label(self.tab_download, text="Resolution:", font=(
            "times", 16)).place(x=10, y=10, width=110, height=50)
        ttk.Label(self.tab_download, text="Progress:", font=(
            "times", 16)).place(x=10, y=420, width=110, height=50)
        self.resolution_select.place(x=120, y=10, width=460, height=400)
        self.dl_progress_bar.place(x=10, y=480, height=25)

    def download_video(self):
        self.dl_progress_bar['value'] = 0
        itag = self.video_download_resolution[self.resolution_select.selection_get(
        )]
        stream = self.streams.get_by_itag(itag)
        save_path = fd.asksaveasfilename(confirmoverwrite=True, filetypes=[(
            f"{stream.mime_type[6:]} file", f"*.{stream.mime_type[6:]}")], initialdir=str(Path(__file__).parent)+"\\downloads", initialfile=f"{stream.title}.{stream.mime_type[6:]}")
        print(os.path.dirname(f"{save_path}.{stream.mime_type[6:]}"))
        if (stream.filesize > LIMIT):
            large_file = mb.askyesno(
                "Warning", f"The file you are trying to download is large ({round(stream.filesize / 1000000, 2)} MB)."
                f"Are you sure you want to continue the download of \"{os.path.basename(save_path)}\"")
            if not large_file:
                print("Cancelled (large size)")
                return
        stream.download(os.path.dirname(save_path),
                        os.path.basename(save_path), "YTDL_")

    def on_complete_dl(self, stream, filepath):
        mb.showinfo("Info", "Download completed!")

    def on_progress_dl(self, stream, chunk, bytes_remaining):
        self.dl_progress_bar['value'] = 100 - (bytes_remaining / stream.filesize * 100)

    def handle_unexpected_close_event(self):
        self.unexpected_kill_flag = True
        self.root.destroy()

    def handle_close_event(self):
        self.root.destroy()


if __name__ == "__main__":
    for _, dirs, _ in os.walk(Path(__file__).parent):
        if "downloads" in dirs:
            break
        os.mkdir(str(Path(__file__).parent) + "\\downloads")
        break
    
    get_link = GET_LINK()

    root = tk.Tk()

    gui = GUI(root, get_link.yt_url_verified)

    root.mainloop()
