import tkinter as tk
import requests
import json
from scripts.constants import *
from pytube import YouTube
from tkinter import ttk
from tkinter import messagebox as mb
from threading import Thread


class GET_LINK:
    def __init__(self) -> None:
        self.root = tk.Tk()
        self.root.protocol("WM_DELETE_WINDOW", self.handle_unexpected_close_event)
        self.root.resizable(width=False, height=False)
        self.root.title("Get Link")
        self.root.geometry(f"{600}x{100}+{int(self.root.winfo_screenwidth() / 2 - 300)}+{int(self.root.winfo_screenheight() / 2 - 50)}")
        
        self.yt_url = tk.StringVar(self.root, "")
        self.yt_url_verified = ""
        
        Thread(target=self.init_widget()).start()
        
    def init_widget(self):
        self.style = ttk.Style()
        self.main_frame = ttk.Frame(self.root, style="Frame1.TFrame", width=600, height=100)
        self.url_input = tk.Entry(self.main_frame, textvariable=self.yt_url, font=("times", 16))
        self.url_confirm = tk.Button(self.main_frame, text="Confirm", font=("times", 16), command=self.check_valid_yt_url)
        
        self.draw_widget()
        self.widget_config()
        self.run()
        
    def widget_config(self):
        self.style.configure("Frame1.TFrame", background="blue")
        
    def draw_widget(self):
        self.main_frame.place(x=0, y=0)
        self.url_input.place(x=10, y=30, width=480, height=40)
        self.url_confirm.place(x=500, y=30, width=90, height=40)
        
    def handle_unexpected_close_event(self):
        self.yt_url_verified = None
        self.root.destroy()
        
    def handle_close_event(self):
        self.root.destroy()
    
    def check_valid_yt_url(self):
        try:
            video_object = YouTube(self.yt_url.get())
        except Exception as e:
            mb.showerror("Error", "Error getting video")
            print(f"Error: {e}")
            self.yt_url_verified = None
            return
        finally:
            id = video_object.video_id
        r = requests.get(f"https://www.googleapis.com/youtube/v3/videos?part=id&id={id}&key={API_KEY}")
        if r.status_code != 200:
            mb.showerror("Error", f"API return an status code: {r.status_code}")
            print("API error")
            self.yt_url_verified = None
        else:
            j = json.loads(r.text)
            if j['items']:
                if j['items'][0]['kind'] == "youtube#video":
                    self.yt_url_verified = self.yt_url.get()
                    print("Valid YT URL")
                    self.handle_close_event()
                else:
                    mb.showerror("Error", f"Not a video or ID ({id}) is wrong. Please use another link")
                    print(f"Invalid type or id ({id}) is wrong")
                    self.yt_url_verified = None
            else:
                mb.showerror("Error", f"ID ({id}) is invalid")
                print(f"ID ({id}) not is valid")
                self.yt_url_verified = None
        
    def run(self):
        self.root.mainloop()